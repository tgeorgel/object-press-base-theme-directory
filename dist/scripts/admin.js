/**
 * Disable ACF field 5e6f6cf052f82, making it read-only 
 */
document.addEventListener("DOMContentLoaded", function() {
    let ipt = document.getElementById("acf-field_5e6f6cf052f82");
    
    if (ipt) {
        ipt.disabled = true;
    }
});