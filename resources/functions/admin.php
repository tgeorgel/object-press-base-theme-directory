<?php

/**
 * This file manage admin hooks
 */

/**
 * Theme configuration class
 */
$theme = OP\Framework\Theme::getInstance();


/**
 * Push admin.js code into script queue
 */
$theme->on('admin_enqueue_scripts', function () {
    wp_enqueue_script('my_custom_script', get_template_directory_uri() . '/dist/scripts/admin.js');
});


/**
 * Display custom notices in backoffice based on wp hook 'admin_notices'
 */
$theme->on('admin_notices', function () {
    if (isset($_SESSION['theme_backoffice_notices'])) {
        $notices = $_SESSION['theme_backoffice_notices'];

        if (is_array($notices) && !empty($notices)) {
            foreach ($notices as $notice) {
                printf(
                    '<div class="%s"><p>%s</p></div>',
                    $notice['type']     ?? 'error',
                    $notice['message']  ?? 'Something went wrong'
                );
            }
        }

        unset($_SESSION['theme_backoffice_notices']);
    }
});


/**
 * Push a notice into array, based on notice type and message.
 *
 * @param string $type    Type of the notice (eg: 'error', 'updated')
 * @param string $message Notice message to display to the user
 *
 * @return void
 */
function pushNotice(string $type = 'error', string $message = 'Something went wrong')
{
    if (!isset($_SESSION['theme_backoffice_notices'])) {
        $_SESSION['theme_backoffice_notices'] = [];
    }

    $_SESSION['theme_backoffice_notices'][] = [
        'type'      => $type,
        'message'   => $message
    ];
}
