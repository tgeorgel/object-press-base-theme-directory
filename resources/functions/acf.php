<?php

/**
 * This file manage acf options
 */

/**
 * Theme configuration class
 */
$theme = OP\Framework\Theme::getInstance();


/**
 * Register ACF options pages
 */
if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title'    => __('Options', '148-options'),
        'menu_title'    => __('Options', '148-options'),
        'menu_slug'     => 'theme-options',
        'capability'    => 'edit_posts',
        'position'      => false,
        'parent_slug'   => '',
        'icon_url'      => 'dashicons-admin-generic',
    ));
}
