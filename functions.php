<?php

// “Silence is the ultimate weapon of power.” ― Charles De Gaulle


require_once __DIR__ . '/vendor/autoload.php';

/**
 * Allows theme configuration
 */
$theme = OP\Framework\Theme::getInstance();


/**
 * Include custom functions PHP files
 */
foreach (glob(__DIR__ .'/resources/functions/*.php') as $filename) {
    include $filename;
}
