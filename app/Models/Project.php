<?php

namespace App\Models;

use App\Models\Abstracts\PostModel;

use App\Models\City;

class Project extends PostModel
{
    /**
     * Wordpress post_type associated to the current model
     */
    public static $post_type = 'project';


    /**
     * Get project types (taxonomy) as name (string)
     *
     * @param int $limit Maximum terms to get
     * @return array
     */
    public function types(int $limit = 5)
    {
        $values = $this->getTaxonomyTerms('type');

        if ($limit != null && count($values) > $limit) {
            $values = array_slice($values, 0, $limit);
        }

        return array_map(function ($e) {
            return $e->name ?? '';
        }, $values);
    }


    public function city()
    {
        $city_id = $this->getField('city');

        if ($city_id && City::belongsToModel($city_id)) {
            return City::find($city_id);
        }

        return null;
    }
}
