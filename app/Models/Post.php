<?php

namespace App\Models;

use App\Models\Abstracts\PostModel;

class Post extends PostModel
{
    /**
     * Wordpress post_type associated to the current model
     */
    public static $post_type = 'post';
}
