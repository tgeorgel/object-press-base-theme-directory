<?php

namespace App\Models\Abstracts;

use OP\Framework\Models\PostModel as BasePostModel;

abstract class PostModel extends BasePostModel
{
    public function communFunction()
    {
        return "This function can be called from all my models ! you called me from " . static::class;
    }
}
