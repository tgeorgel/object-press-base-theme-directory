<?php

namespace App\Models;

use App\Models\Abstracts\PostModel;

class Page extends PostModel
{
    /**
     * Wordpress post_type associated to the current model
     */
    public static $post_type = 'page';
}
