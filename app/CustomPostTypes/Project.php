<?php

namespace App\CustomPostTypes;

use OP\Framework\Boilerplates\CustomPostType;

class Project extends CustomPostType
{
    protected static $domain;

    protected static $cpt = 'project';

    /**
     * Singular and plural names of CPT
     */
    public static $singular = 'Project';
    public static $plural   = 'Projects';

    /**
     * Used to display 'un' or 'une'
     */
    public static $is_female = true;
    
    /**
     * Enable graphql
     */
    public static $graphql_enabled = true;


    /**
     * Class constructor, register CTP to wordpress
     */
    public function __construct($domain)
    {
        static::$domain = $domain;

        $args_override = [
            'menu_icon' => 'dashicons-archive',
            'rewrite'   => array('slug' => 'project')
        ];

        $labels_override = [];

        static::register($args_override, $labels_override);
    }
}
