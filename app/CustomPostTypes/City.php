<?php

namespace App\CustomPostTypes;

use OP\Framework\Boilerplates\CustomPostType;

class City extends CustomPostType
{
    protected static $domain;

    protected static $cpt = 'city';

    /**
     * Singular and plural names of CPT
     */
    public static $singular = 'City';
    public static $plural   = 'Cities';

    /**
     * Used to display 'un' or 'une'
     */
    public static $is_female = true;

    /**
     * Enable graphql
     */
    public static $graphql_enabled = true;


    /**
     * Class constructor, register CTP to wordpress
     */
    public function __construct($domain)
    {
        static::$domain = $domain;

        $args_override = [
            'public' => false,
            'publicly_queryable' => false,
            'menu_icon' => 'dashicons-admin-site-alt',
        ];

        $labels_override = [];

        static::register($args_override, $labels_override);
    }
}
