<?php

namespace App\Interfaces;

interface ICpts
{
    const MODELS = [
        'page'    => 'App\Models\Page',
        'post'    => 'App\Models\Post',
        'city'    => 'App\Models\City',
        'project' => 'App\Models\Project',
    ];
}
