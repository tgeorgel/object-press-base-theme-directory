<?php

namespace App\Classes;

use \Jenssegers\Blade\Blade as BladeCore;

if (! class_exists('\App\Classes\Blade')) :

    class Blade extends BladeCore
    {
        private static $instance;

        public static function getInstance()
        {
            if (!isset(self::$instance)) {
                self::$instance = new self();
            }

            return self::$instance;
        }

        private function __construct()
        {
            $views_path = get_template_directory() . '/views';
            $cache_path = wp_upload_dir()['basedir'] . '/blade__cache';

            if (!is_dir($cache_path)) {
                mkdir($cache_path);
            }
            
            parent::__construct($views_path, $cache_path);
        }

        public static function template(string $view, array $data = [], array $mergeData = []): string
        {
            return self::getInstance()->render($view, $data, $mergeData);
        }
    }

endif;
