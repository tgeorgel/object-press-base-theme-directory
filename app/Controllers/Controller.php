<?php

namespace App\Controllers;

use App\Classes\Blade;

abstract class Controller
{
    public function render(string $view, array $data = [])
    {
        echo Blade::template($view, $data);
    }
}
