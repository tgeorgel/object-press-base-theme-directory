<?php

namespace App\Controllers;

use App\Models\Project;
use OP\Framework\Helpers\AcfHelper;

class ProjectController extends Controller
{
    /**
     * View a single Project
     */
    public static function view(int $id)
    {
        if (!Project::belongsToModel($id)) {
            wp_redirect(home_url());
            die;
        }

        $this->render('project.view', [
            'project' => Project::find($id)
        ]);
    }
}
