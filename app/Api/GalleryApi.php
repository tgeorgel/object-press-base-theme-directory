<?php

namespace App\Api;

use App\Helpers\GalleryHelper;

class GalleryApi
{
    public static function init()
    {
        // GET /
        // eg: /wp-json/gallery/v1
        register_rest_route('gallery/v1', '/all', array(
            'methods' => 'GET',
            'callback' => array(self::class, 'retreiveGallery'),
            'args' => array()
        ));
    }


    /**
     * Grep the gallery and return it
     */
    public static function retreiveGallery(\WP_REST_Request $request)
    {
        return GalleryHelper::getGallery();

        // array[array[
        //     'image_id'      => int,
        //     'image_link'    => string,
        //     'project_title' => string,
        //     'project_link'  => string,
        //     'project_slug'  => string,
        //     'filters'       => array[ string, string ]
        // ], array[...]]
    }
}
