<?php

namespace App\Taxonomies;

use OP\Framework\Boilerplates\Taxonomy;

class Type extends Taxonomy
{
    protected static $domain;

    protected static $taxonomy = 'type';


    /**
     * Singular and plural names of CPT
     *
     * @var string
     */
    public static $singular = 'Type';
    public static $plural   = 'Types';


    /**
     * Enable graphql
     *
     * @var bool
     */
    public static $graphql_enabled = true;


    /**
     * Post types that will have the taxonomy
     *
     * @var array
     */
    protected static $post_types = [
        'project',
    ];

    /**
     * Class constructor, register CTP to wordpress
     */
    public function __construct($domain)
    {
        static::$domain = $domain;

        $args_override   = [];
        $labels_override = [];

        static::register($args_override, $labels_override);
    }
}
