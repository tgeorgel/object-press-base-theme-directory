<?php

namespace App\Helpers;

use App\GraphQL\Types\GalleryItem;
use OP\Framework\Helpers\PostHelper;

use App\Models\Attachment;
use App\Models\Project;

class GalleryHelper
{
    /**
     * Return the gallery as formated array
     * 
     * @return array
     */
    public static function getGallery()
    {
        $attachments = static::getWithParent();
        $formated = [];

        foreach ($attachments as $element) {
            /**
             * Same as PostHelper::isA($element->parent_id, 'project')
             */
            if (!Project::belongsToModel($element->parent_id)) {
                continue;
            }

            $project = new Project($element->parent_id);

            $data = [
                'image_id'       => $project->getThumbnailId(),
                'image_link'     => $project->getThumbnailUrl(),
                'project_title'  => $project->title,
                'project_link'   => $project->permalink(),
                'project_slug'   => $project->name,
                'filters_city'   => [$project->city()],
                'filters_types'  => $project->types(),
            ];

            $formated[] = $data;
        }

        return $formated;
    }


    /**
     * Get attachment that has a parent post
     * 
     * @return array
     */
    public static function getWithParent()
    {
        global $wpdb;

        $query = "  SELECT      $wpdb->posts.ID as attachment_id, $wpdb->posts.post_parent as parent_id, wp_term_taxonomy.taxonomy, wp_terms.slug as term
                    FROM        $wpdb->posts
                        INNER JOIN wp_term_relationships ON wp_posts.ID = wp_term_relationships.object_id
                        INNER JOIN wp_term_taxonomy ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
                        INNER JOIN wp_terms ON wp_term_taxonomy.term_id = wp_terms.term_id
                    WHERE       $wpdb->posts.post_parent != 0
                    AND         $wpdb->posts.post_type IN ('attachment')
                    ORDER BY $wpdb->posts.ID DESC
                    LIMIT 100;
        ";

        $results = $wpdb->get_results($query);

        return $results;
    }
}
