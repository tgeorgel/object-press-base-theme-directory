<?php

namespace App\GraphQL\Fields;

use OP\Framework\GraphQL\GqlField;
use App\Helpers\GalleryHelper;

/**
 * Field specifications :
 *
 * This field is returning the gallery filters
 */
class GalleryFilters extends GqlField
{
    public static $field_name = 'galleryFilters';

    public static $field_type = ['list_of' => 'GalleryFilter'];

    public static $field_description = 'The gallery filters';

    public static $targets = [
        'Page'
    ];


    /**
     * GraphQL resolve callback
     *
     * @param \WP_Post $post
     *
     * @return string
     */
    public static function resolve($post)
    {
        $resolve = [
            'type' => [
                'filter' => 'type',
                'values' => [],
            ],
        ];

        // Get taxonomies terms
        $terms = get_terms([
            'taxonomy'   => ['type'],
            'hide_empty' => false,
        ]);

        // Push terms names in filters values
        foreach ($terms as $term) {
            $resolve[$term->taxonomy]['values'][] = $term->name;
        }

        return $resolve;
    }
}
