<?php

namespace App\GraphQL\Fields;

use OP\Framework\GraphQL\GqlField;
use App\Helpers\GalleryHelper;

/**
 * Field specifications :
 *
 * This field is returning the gallery items
 */
class GalleryItems extends GqlField
{
    public static $field_name = 'galleryItems';

    public static $field_type = ['list_of' => 'GalleryItem'];

    public static $field_description = 'The gallery items';

    public static $targets = [
        'Page'
    ];


    /**
     * GraphQL resolve callback
     *
     * @param \WP_Post $post
     *
     * @return string
     */
    public static function resolve($post)
    {
        return GalleryHelper::getGallery();
    }
}
