<?php

namespace App\GraphQL\Types;

use OP\Framework\GraphQL\GqlType;

/**
 * Field specifications :
 *
 * This type define a Gallery filter architecture
 */
class GalleryFilter extends GqlType
{
    public static $type_name = 'GalleryFilter';

    public static $type_description = 'A gallery filter to be used in the gallery area';

    public static $fields = [
        'filter' => [
            'type'          => 'String',
            'description'   => 'The filter slug.',
        ],
        'values' => [
          'type'            => ['list_of' => 'String'],
          'description'     => "The filter entries available",
        ],
    ];
}
