<?php

namespace App\GraphQL\Types;

use OP\Framework\GraphQL\GqlType;

/**
 * Field specifications :
 *
 * This Type define the gallery item architecture
 */
class GalleryItem extends GqlType
{
    public static $type_name = 'GalleryItem';

    public static $type_description = 'A gallery item to be used in the gallery area';

    public static $fields = [
        'image_id' => [
            'type'          => 'Integer',
            'description'   => 'The image id.',
        ],
        'image_link' => [
            'type'          => 'String',
            'description'   => 'The image url.',
        ],
        'project_title' => [
            'type'            => 'String',
            'description'     => "The image project's title.",
        ],
        'project_link' => [
            'type'            => 'String',
            'description'     => "The image project's url.",
        ],
        'project_slug' => [
            'type'            => 'String',
            'description'     => "The image project's slug.",
        ],
        'filters_city' => [
            'type'            => ['list_of' => 'String'],
            'description'     => "City filter data.",
        ],
        'filters_types' => [
            'type'            => ['list_of' => 'String'],
            'description'     => "Types filter data.",
        ],
    ];
}
